

#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"
#include "analog.h"
#include "console.h"
#include "nvs_flash.h"

#define LOG_LOCAL_LEVEL ESP_LOG_ERROR
#include "esp_log.h"


#define TAG "app_main"

void app_main(void)
{
	esp_err_t ret;

	// Initialize NVS.
	ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
	    ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK( ret );


	esp_log_level_set(TAG, ESP_LOG_DEBUG);
	ESP_LOGD(TAG, "Enter app main");
    //Allow other core to finish initialization
    vTaskDelay(pdMS_TO_TICKS(100));

    console_init();

    analog_init();
}
