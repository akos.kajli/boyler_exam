#include <time.h>
#include <string.h>
#include <stdbool.h>
#include "driver/adc.h"

#include "analog.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"
#include "freertos/semphr.h"


#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
//#define LOG_LOCAL_LEVEL ESP_LOG_ERROR
#include "esp_log.h"

#define TAG "analog"

#define ANALOG_STACK_SIZE (4096)
#define ANALOG_TASK_DELAY_MS (100)

#define MOVING_AVERAGE_QUEUE_SIZE (10)
#define MOVING_HYSTERESIS_DELTA (10)

#define CHANNELS (2)
#define MIN_DIGITAL (0)
#define MAX_DIGITAL (4096)

#define T_MIN_TEMPERATURE (0)
#define T_MAX_TEMPERATURE (100)

#define MIN_ON_TIME (300000) //5 min for both
#define MIN_OFF_TIME (300000)

#define MIN(a,b) ((a)<(b)?(a):(b))

#define MAX(a,b) ((a)>(b)?(a):(b))

typedef struct{
	uint16_t min;
	uint16_t max;
	uint16_t delta;
}moving_hysteresis_t;

typedef struct{
	uint16_t queue[MOVING_AVERAGE_QUEUE_SIZE];
	uint8_t index;
}moving_average_t;

typedef struct {
	uint16_t d0;
	uint16_t d1;
	float t0;
	float t1;
	float tg_alpha;
	uint8_t verified;
}verification_t;

typedef struct {
	float tb;
	float tf;
}boyler_t;

typedef struct{
	uint16_t raw;
	uint16_t normalized;
	float temp;
	moving_hysteresis_t moving_hyst;
	moving_average_t moving_avg;
	verification_t verification;
	boyler_t boyler;
}ad_channel_t;

static uint8_t ch_def[CHANNELS]={
		ADC1_CHANNEL_6,			//gpio 34
		ADC1_CHANNEL_7			//gpio 35
};

static TaskHandle_t ad_task_handler;
static TaskHandle_t control_task_handler;
static ad_channel_t ad_chn[CHANNELS];


static inline BaseType_t check_channel(uint8_t ch){
	if(ch>CHANNELS){
		ESP_LOGD(TAG, "channel range error %d/%d", ch, CHANNELS);
		return pdFALSE;
	}
	return pdTRUE;
}

static uint16_t get_moving_hysteresis(uint16_t input, uint8_t ch){
	if(input>ad_chn[ch].moving_hyst.max){
		ad_chn[ch].moving_hyst.max=MIN(AD_MAX, ad_chn[ch].moving_hyst.max+ad_chn[ch].moving_hyst.delta);
		ad_chn[ch].moving_hyst.min=ad_chn[ch].moving_hyst.max-(ad_chn[ch].moving_hyst.delta*2);
		return ad_chn[ch].moving_hyst.max;
	}
	if(input<ad_chn[ch].moving_hyst.min){
		if(ad_chn[ch].moving_hyst.min>AD_MIN+ad_chn[ch].moving_hyst.delta){
			ad_chn[ch].moving_hyst.min-=ad_chn[ch].moving_hyst.delta;
			ad_chn[ch].moving_hyst.max=ad_chn[ch].moving_hyst.min+(ad_chn[ch].moving_hyst.delta*2);
		}
		return ad_chn[ch].moving_hyst.min;
	}

	return input;
}

static uint16_t get_moving_average(uint16_t input, uint8_t ch){
	ad_chn[ch].moving_avg.queue[ad_chn[ch].moving_avg.index]=input;
	ad_chn[ch].moving_avg.index=(ad_chn[ch].moving_avg.index+1)%MOVING_AVERAGE_QUEUE_SIZE;

	uint32_t sum=0;

	for(uint8_t i=0; i<MOVING_AVERAGE_QUEUE_SIZE; i++){
		sum+=ad_chn[ch].moving_avg.queue[i];
	}
	return sum/MOVING_AVERAGE_QUEUE_SIZE;
}

static void fn_analog(void* args){
	ESP_LOGD(TAG, "Enter fn_analog");
	while(1){
		for(int i=0; i<CHANNELS; i++){
				ad_chn[i].raw=adc1_get_raw(ch_def[i]);
				ad_chn[i].normalized=get_moving_average(get_moving_hysteresis(ad_chn[i].raw, i),i);
				ad_chn[i].temp=temperature_get(i,ad_chn[i].normalized);
		}
		vTaskDelay(pdMS_TO_TICKS(ANALOG_TASK_DELAY_MS));
	}
}

BaseType_t analog_get(uint16_t *result,uint8_t ch, BaseType_t wait_tick){
	*result=ad_chn[ch].normalized;
	return pdTRUE;
}

static void calc_tg_alpha(uint8_t ch){
	if(check_channel(ch)==pdFALSE){
		return;
	}
	ad_chn[ch].verification.d0=analog_get(&ad_chn[0].verification.d0, ch,100);
	ad_chn[ch].verification.d1=analog_get(&ad_chn[1].verification.d1,ch, 100);
	ad_chn[ch].verification.t0=T_MIN_TEMPERATURE;
	ad_chn[ch].verification.t1=T_MAX_TEMPERATURE;

	verification_t* v=&ad_chn[ch].verification;
	if (v->d1>v->d0 &&
		v->t1>v->t0 &&
		v->d1<=MAX_DIGITAL &&
		v->t0>=T_MIN_TEMPERATURE &&
		v->t1<=T_MAX_TEMPERATURE){
		v->tg_alpha=(v->t1-v->t0) / (float)(v->d1-v->d0);
		v->verified=1;
	}
}

float temperature_get(uint8_t ch, uint16_t dig_in){
	if(check_channel(ch)==pdFALSE){
		return 0.0f;
	}

	calc_tg_alpha(ch);

	if(ad_chn[ch].verification.verified){
		printf("channel %d isn't verified yet\n", ch);
		return 0.0f;
	}

	verification_t* v=&ad_chn[ch].verification;
	return v->t0+(dig_in-v->d0)*v->tg_alpha;
}



BaseType_t temp_get_value(uint8_t ch, float *val){
	*val=ad_chn[ch].temp;
	return pdTRUE;
}

#define TEMP_DIFF (5)
#define PUMP_CTRL GPIO_NUM_5

typedef struct{
	time_t last_on;
	time_t last_off;
	bool pump_on;
}boyler_stats_t;

boyler_stats_t boyler;

static void fn_control(void* args){
	ESP_LOGD(TAG, "Enter fn_control");
	float temp1=0;
	float temp2=0;
	while(1){
		time_t now;
		time(&now);

		temp1=temp_get_value(0, &ad_chn[0].boyler.tf);
		temp2=temp_get_value(1, &ad_chn[1].boyler.tb);

		if(temp2>temp1+TEMP_DIFF){
			if(boyler.pump_on==false){
				if(difftime(now, boyler.last_on)>MIN_OFF_TIME){
				gpio_set_level(PUMP_CTRL, 1);
				boyler.pump_on=true;
				time(&boyler.last_on);}
			}
		}else{
			if(boyler.pump_on==true){
				if(difftime(now, boyler.last_off)>MIN_ON_TIME){
					gpio_set_level(PUMP_CTRL, 0);
					boyler.pump_on=false;
					time(&boyler.last_off);
				}
			}
		}
		vTaskDelay(pdMS_TO_TICKS(ANALOG_TASK_DELAY_MS));
	}
}

//Pump commands
//get temp values, get pump state and time, force on and force off
// boyler <temp|state|force_on|force_off> <channel>

#define CMD_BOYLER "boyler"
#define HINT_BOYLER "temp/state/force_on/force_off"
#define HELP_BOYLER "boyler base commands: temp\tstate\tforce_on\tforce_off\thelp\n" 	\
					"boyler short commands: t\ts\tn\t\tf\t\th\n" 	\
					"	Example \n"				\
					"		boyler --help \n" \
					"		boyler --channel 0 --temp \n" \
					"		boyler -c 1 -t\n"

#define B_ARG_TEMP "temp"
#define B_HINT_TEMP "get raw and normalized temperature for specified channel"

#define B_ARG_CHANNEL "channel"
#define B_HINT_CHANNEL "set channel"

#define B_ARG_STATE "state"
#define B_HINT_STATE "get boyler status"

#define B_ARG_FORCE_ON "force_on"
#define B_HINT_FORCE_ON "force the boyler to turn on if it can"

#define B_ARG_FORCE_OFF "force_off"
#define B_HINT_FORCE_OFF "force the boyler to turn off if it can"

#define DATA_TYPE "<n>"

static struct{
	struct arg_lit *help;
	struct arg_lit *temp;
	struct arg_lit *state;
	struct arg_lit *force_on;
	struct arg_lit *force_off;
	struct arg_int *channel;
	struct arg_end *end;
}boyler_args;

static int cmd_boyler(int argc, char** argv){

	int nerr=arg_parse(argc, argv, (void**) &boyler_args);

	if(nerr || argc==1 || boyler_args.help->count>0){

		printf(HELP_BOYLER);
		return 0;
	}
	if(boyler_args.channel->count==0){
		printf("you always have to set channel\n");
		return 1;
	}

	uint8_t ch=boyler_args.channel->ival[0];
	if(check_channel(ch)==pdFAIL){
		printf("Channel index maximum: %d\n", CHANNELS);
		return 1;
		}
	if(boyler_args.temp->count>0){
		printf("The measured raw value is: %d, normalized value is: %d, and temperature value is: %.2f\n",
				ad_chn[ch].raw, ad_chn[ch].normalized, ad_chn[ch].temp);
		return 0;
	}
	if(boyler_args.state->count>0){
		time_t now;
		time(&now);
		printf("The pump is in state: %s \n",
				boyler.pump_on ? "On":"Off");
		if(boyler.pump_on){
			printf("It has been in this state for %.2f seconds\n", difftime(now, boyler.last_on));
			return 0;
		}else{
			printf("It has been in this state for %.2f seconds\n", difftime(now, boyler.last_off));
			return 0;
		}
	}
	if(boyler_args.force_on->count>0){
		if(!boyler.pump_on){
			boyler.pump_on=true;
			gpio_set_level(PUMP_CTRL, 1);
			printf("The boyler has been forced on\n");
			return 0;
		}
		printf("The boyler is already on!\n");
		return 1;
	}
	if(boyler_args.force_off->count>0){
		if(boyler.pump_on){
			boyler.pump_on=false;
			gpio_set_level(PUMP_CTRL, 0);
			printf("The boyler has been forced off\n");
			return 0;
		}
		printf("The boyler is already off!\n");
		return 1;
	}
	return 1;
}

static void register_boyler_cmd(){
	boyler_args.help=arg_lit0("hH", HELP_BOYLER, HINT_BOYLER);
	boyler_args.temp=arg_litn("tT", B_ARG_TEMP, 0, 1, B_HINT_TEMP);
	boyler_args.state=arg_litn("sS", B_ARG_STATE, 0, 1, B_HINT_STATE);
	boyler_args.force_on=arg_litn("nN", B_ARG_FORCE_ON, 0, 1, B_HINT_FORCE_ON);
	boyler_args.force_off=arg_litn("fF", B_ARG_FORCE_OFF, 0, 1, B_HINT_FORCE_OFF);
	boyler_args.channel=arg_intn("cC", B_ARG_CHANNEL,DATA_TYPE, 0, 1, B_HINT_CHANNEL);
	boyler_args.end=arg_end(0);

	const esp_console_cmd_t cmd={
			.command=CMD_BOYLER,
			.help=HELP_BOYLER,
			.hint=HINT_BOYLER,
			.func=&cmd_boyler
	};

	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}


void analog_init(){
	bzero(&ad_chn, (sizeof(ad_chn)));
	for(int i=0; i<CHANNELS; i++){
	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(ch_def[i], ADC_ATTEN_6db);


	uint16_t raw=adc1_get_raw(ch_def[i]);
	ad_chn[i].moving_hyst.min=raw<ad_chn[i].moving_hyst.delta? 0: raw-MOVING_HYSTERESIS_DELTA;
	ad_chn[i].moving_hyst.max=raw>raw+MOVING_HYSTERESIS_DELTA;
	ad_chn[i].moving_hyst.delta=MOVING_HYSTERESIS_DELTA;
	}
	register_boyler_cmd();
	xTaskCreate(fn_control, "control", ANALOG_STACK_SIZE, NULL, uxTaskPriorityGet(NULL), &control_task_handler);
	xTaskCreate(fn_analog, "analog", ANALOG_STACK_SIZE, NULL, uxTaskPriorityGet(NULL), &ad_task_handler);

}
