/*
 * analog.h
 *
 *  Created on: 2020. nov. 17.
 *      Author: Bob
 */

#ifndef MAIN_ANALOG_H_
#define MAIN_ANALOG_H_

#include <stdint.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define AD_MAX (4096)
#define AD_MIN (0)

void analog_init();

BaseType_t analog_get(uint16_t *result, uint8_t ch,BaseType_t wait_tick);
float temperature_get(uint8_t ch, uint16_t dig_in);

#endif /* MAIN_ANALOG_H_ */
