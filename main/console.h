	/*
 * console.h
 *
 *  Created on: 2020. nov. 25.
 *      Author: Bob
 */

#ifndef MAIN_CONSOLE_H_
#define MAIN_CONSOLE_H_


#define CONSOLE_TASK_SIZE (4096)

#define CONSOLE_HIST_LINE_SIZE (100)

void console_init();
#endif
